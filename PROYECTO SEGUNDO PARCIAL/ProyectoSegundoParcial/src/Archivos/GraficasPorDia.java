/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Juampiece
 */
public class GraficasPorDia  {

    
    public  static Pane getGraficaDia() {

        CategoryAxis yAxis = new CategoryAxis();
        yAxis.setLabel("Dias de entrega");

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Numero de Entregas");

        BarChart chart = new BarChart(xAxis, yAxis);
        chart.setTitle("GRAFICAS TOTAL");
        chart.setData(getDataHSeries());

        StackPane root = new StackPane();
        root.getChildren().add(chart);

        Scene scene = new Scene(root, 640, 427);

        //primaryStage.setTitle("GRAFICOS CANGURO S.A");
        //primaryStage.setScene(scene);
        //primaryStage.show();
        
        
        
        return root;
    }
    
    public static ObservableList<XYChart.Series<Number, String>> getDataHSeries() {

        XYChart.Series<Number, String> EntregaXdia = new XYChart.Series<>();
        EntregaXdia.setName("Numero de entregas por dia");
        EntregaXdia.getData().addAll(
                new XYChart.Data<>(12, "Lunes"),
                new XYChart.Data<>(54, "Martes"),
                new XYChart.Data<>(12, "Miercoles"),
                new XYChart.Data<>(10, "Jueves"),
                new XYChart.Data<>(158, "Viernes"));

        ObservableList<XYChart.Series<Number, String>> data = FXCollections.observableArrayList();
        data.add(EntregaXdia);
        return data;
        
    }

}