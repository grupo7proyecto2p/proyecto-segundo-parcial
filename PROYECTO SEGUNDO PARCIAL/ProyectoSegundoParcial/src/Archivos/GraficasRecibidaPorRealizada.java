/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archivos;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Juampiece
 */
public class GraficasRecibidaPorRealizada {

   
    public static Pane getPane() {

        CategoryAxis yAxis = new CategoryAxis();
        yAxis.setLabel("Dias de entrega");

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Numero de Entregas");

        BarChart chart = new BarChart(xAxis, yAxis);
        chart.setTitle("GRAFICAS TOTAL");
        chart.setData(getDataHSeries());

        StackPane root = new StackPane();
        root.getChildren().add(chart);

        /*Scene scene = new Scene(root, 640, 427);

        primaryStage.setTitle("GRAFICOS CANGURO S.A");
        primaryStage.setScene(scene);
        primaryStage.show(); */
        
        return root;
    }
    
    public static ObservableList<XYChart.Series<Number, String>> getDataHSeries() {
    XYChart.Series<Number, String> ReciXreali = new XYChart.Series<>();
        ReciXreali.setName("Numero de entregas por dia");
        ReciXreali.setName("Entregas recibidas vs entregas realizadas");
        ReciXreali.getData().addAll(
                new XYChart.Data<>(107, "Recibidas"),
                new XYChart.Data<>(100, "Realizadas"),
                new XYChart.Data<>(590, "Recibidas"),
                new XYChart.Data<>(485, "Realizadas"));
        
        ObservableList<XYChart.Series<Number, String>> data = FXCollections.observableArrayList();
        data.add(ReciXreali);
        return data;
      }
  
  }
