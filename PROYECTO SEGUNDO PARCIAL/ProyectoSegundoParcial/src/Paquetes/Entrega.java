/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paquetes;

import Entidades.Conductor;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import Mapas.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juampiece
 */
public class Entrega {
    
    private StringProperty id;
    private StringProperty cliente;
    private Conductor conductor;
    private StringProperty estado;
    private StringProperty fechaIngreso;
    private StringProperty fechaEntrega;
    private double lat;
    private double lon;
    private String dimensiones;
    private String direccion;
    
    

    public Entrega(String id, String cliente, Conductor conductor, String estado, String fechaIngreso, String fechaEntrega, double lat, double lon, String dimensiones) {
        this.id = new SimpleStringProperty(id);
        this.cliente = new SimpleStringProperty(cliente);
        this.conductor = conductor;
        this.estado = new SimpleStringProperty(estado);
        this.fechaIngreso = new SimpleStringProperty(fechaIngreso);
        this.fechaEntrega = new SimpleStringProperty(fechaEntrega);
        this.lat = lat;
        this.lon = lon;
        this.dimensiones = dimensiones;
                
    }

    public Entrega() {
    }

    public StringProperty getId() {
        return id;
    }

    public void setId(StringProperty id) {
        this.id = id;
    }

    public StringProperty IdProperty(){
        return this.id;
    }

    public Conductor getConductor() {
        return conductor;
    }

    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }

    public String getCliente() {
        return cliente.get();
    }

    public StringProperty clienteProperty(){
        return this.cliente;
    }
    public void setCliente(String cliente) {
        this.cliente.set(cliente);
    }

    public StringProperty getEstado() {
        return estado;
    }
    
    public String getSEstado(){
        return this.estado.get();
    }

    public void setEstado(String estado) {
        this.estado.set(estado);
    }

    

    public LocalDate getFechaIngreso(String fecha) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        return LocalDate.parse(fecha, formatter);
    }
    //los getS sirven para ayudar en la busqueda de datos en la tabla.
     public String getSFechaIngreso(){
        return this.fechaIngreso.get();
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso.set(fechaIngreso);
    }
    
    public StringProperty fechaIngresoProperty(){
        return this.fechaIngreso;
    }
    
    //los getS sirven para ayudar en la busqueda de datos en la tabla.
    public String getSFechaEntrega(){
        return this.fechaEntrega.get();
    }

    public LocalDate getFechaEntrega(String fecha) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(fecha, formatter);
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega.set(fechaEntrega);
    }
    
    public StringProperty fechaEntregaProperty(){
        return this.fechaEntrega;
    }
    

    public double getLat() {
        return lat;
    }

        public String getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(String dimensiones) {
        this.dimensiones = dimensiones;
    }
    
    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
    public String getdireccion(){
        try {
            String perso="";
            Geocoding global = new Geocoding();
            ArrayList<String> per =global.ObtnerDireccion(lat, lon);
            for( String p:per){
                 perso+= p;
            }
            return perso;
        } catch (UnsupportedEncodingException | MalformedURLException ex) {
            Logger.getLogger(Entrega.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
       
       
    }

    
    
    
}
