/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mapas;

/**
 *
 * @author Juampiece
 */

import java.io.IOException;
import javafx.geometry.Point2D;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * El uso del API de codificación geográfica de Google está sujeto a un límite de 2.500 solicitudes de codificación geográfica al día.<br/>
 * El API de codificación geográfica solo se puede utilizar en combinación con un mapa de Google.
 */
public class Geocoding extends MapsJava{

    private final String URLRaiz="http://maps.google.com/maps/api/geocode/xml";
    private final String pathEstado="GeocodeResponse/status";
    private final String pathCodPost="GeocodeResponse/result/address_component";
    
    private String DirecEncon;
    private String CodigoPos;
    
    /**
     * Devuelve la dirección encontrada a partir de la enviada como parámetro en la función ObtenerCoordenadas.
     * <b>REQUIERE PRIMERAMENTE HACER PETICIÓN DE COORDENADAS (ObtenerCoordenadas).</b>
     * @return devuelve la dirección encontrada.
     * En caso de no encontrar dirección, devuelve "No data"
     */
    public String getAddressFound() {
        return DirecEncon;
    }
   
    public String getPostalcode() {
        return CodigoPos;
    }

    @Override
    protected void onError(URL urlRequest, String status, Exception ex) {
        super.storageRequest(urlRequest.toString(), "Geocoding request", status, ex);
    }

    @Override
    protected String getStatus(XPath xpath, Document document) {
        NodeList nodes;
        try {
            nodes = (NodeList) xpath.evaluate(this.pathEstado, 
                document, XPathConstants.NODESET);
            return nodes.item(0).getTextContent();
        } catch (XPathExpressionException ex) {
            return null;
        }
    }
    
    @Override
    protected void storeInfoRequest(URL urlRequest, String info, String status, Exception exception) {
        super.storageRequest(urlRequest.toString(), "Geocoding request", status, exception);
    }

    private String getNodesPostalcode(NodeList node){
         String result="No data";
         int i=0;
         while (i<node.getLength()) {
            String nodeString = node.item(i).getTextContent();
            if(nodeString.contains("postal_code")){
                result=nodeString.replace(" ", "").substring(1,6);
                break;
            }
            i+=1;
        }
        return result;
    }

    private URL createURL(String address) throws UnsupportedEncodingException, MalformedURLException{
        URL urlReturn=new URL(URLRaiz + "?address=" + URLEncoder.encode(address, "utf-8") + super.getSelectPropertiesRequest());
        return urlReturn;
    }
    
    private URL createURL(Double latitude, Double longitude) throws UnsupportedEncodingException, MalformedURLException{
        URL urlReturn=new URL(URLRaiz + "?latlng=" + latitude + "," + longitude + super.getSelectPropertiesRequest());
        return urlReturn;
    }
    
    /**
     * Esta función transforma una dirección especificada (como "Espol, Obelisco"), en coordenadas geográficas
     * (-2.147898, -79.964962).
     * @param address dirección postal que se quiere codificar de forma geográfica
     * @return devuelve un Point2D.Double donde la "x" es latitud y la "y" es longitud.
     * Devuelve 0.0 o null en caso de error.
     * @throws java.io.UnsupportedEncodingException
     * @throws java.net.MalformedURLException
     */
    public Point2D ObtenerCoordenadas(String address)throws UnsupportedEncodingException, MalformedURLException{
        this.DirecEncon="";
        
        URL url=createURL(address);
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
                DocumentBuilder builder = factory.newDocumentBuilder(); 
                Document document = builder.parse(url.openStream()); 

                XPathFactory xpathFactory = XPathFactory.newInstance(); 
                XPath xpath = xpathFactory.newXPath(); 

                NodeList nodeLatLng = (NodeList) xpath.evaluate("GeocodeResponse/result/geometry/location[1]/*", 
                         document, XPathConstants.NODESET);
                NodeList nodeAddress = (NodeList) xpath.evaluate("GeocodeResponse/result/formatted_address", 
                         document, XPathConstants.NODESET);
                NodeList nodePostal = (NodeList) xpath.evaluate(this.pathCodPost, 
                         document, XPathConstants.NODESET);
                
                Double lat=0.0;
                Double lng=0.0;
                try {
                    this.CodigoPos=this.getNodesPostalcode(nodePostal);
                    this.DirecEncon="No data";
                    this.DirecEncon=nodeAddress.item(0).getTextContent();
                    lat = Double.valueOf(nodeLatLng.item(0).getTextContent());
                    lng = Double.valueOf(nodeLatLng.item(1).getTextContent());
                } catch (NumberFormatException | DOMException e) {
                     onError(url,"NO STATUS",e);
                }
                
                Point2D result = new Point2D(lat, lng);
                this.storeInfoRequest(url, null, this.getStatus(xpath, document), null);
                return result;
            } catch (IOException | ParserConfigurationException | XPathExpressionException | SAXException e) {
                onError(url,"NO STATUS",e);
                return null;
            }
     }
    
    /**
     * Esta función "traduce" coordenadas geográficas (como 40.4171111,-3.7031133), en una dirección postal ("Espol")
     * @param latitude latitud del punto a codificar
     * @param longitude longitud del punto a codificar
     * @return devuelve un ArrayList<String> con todas las direcciones postales asociadas al punto especificado. Índices
     * menores del ArrayList, especifican direcciones postales más específicas.
     * Devuelve null en caso de error.
     * @throws java.io.UnsupportedEncodingException
     * @throws java.net.MalformedURLException
     * 
     */
    public ArrayList<String>  ObtnerDireccion(Double latitude, Double longitude) throws UnsupportedEncodingException, MalformedURLException{
        URL url=createURL(latitude,longitude);
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
                DocumentBuilder builder = factory.newDocumentBuilder(); 
                Document document = builder.parse(url.openStream()); 

                XPathFactory xpathFactory = XPathFactory.newInstance(); 
                XPath xpath = xpathFactory.newXPath(); 

                NodeList nodeAddress = (NodeList) xpath.evaluate("GeocodeResponse/result/formatted_address", 
                         document, XPathConstants.NODESET);
                NodeList nodePostal = (NodeList) xpath.evaluate(this.pathCodPost, 
                         document, XPathConstants.NODESET);
                
                ArrayList<String> result=super.getNodesString(nodeAddress);
                this.CodigoPos=this.getNodesPostalcode(nodePostal);
                
                this.storeInfoRequest(url, null, this.getStatus(xpath, document), null);
                
                
                return result;
                
            } catch (IOException | ParserConfigurationException | XPathExpressionException | SAXException e) {
                onError(url,"NO STATUS",e);
                return null;
            }
     }
    
   }
