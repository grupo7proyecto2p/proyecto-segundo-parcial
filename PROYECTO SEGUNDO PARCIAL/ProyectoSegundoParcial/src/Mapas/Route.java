/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mapas;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * El uso del API de rutas de Google está sujeto a un límite de 2.500 solicitudes de rutas al día.
 */
public class Route extends MapsJava{

    private final String URLRoot="http://maps.googleapis.com/maps/api/directions/xml";
    private final String pathStatus="DirectionsResponse/status";
    
    private String summary="";
    private String copyright="";
    private ArrayList<Integer> waypointIndex=new ArrayList<>();
    private ArrayList<Integer> totalTime=new ArrayList<>();
    private ArrayList<Integer> totalDistance=new ArrayList<>();
    private ArrayList<String> polilines=new ArrayList<>();
    
    /**
     * Indica el tipo de medio de transporte de la ruta.
     */
    public enum mode{driving,walking,bicycling,transit}
    
    /**
     * Indica que la ruta deben evitar determinados elementos. 
     * 
     */
    public enum avoids{nothing,tolls,highways}

    /**
     * Indica el resumen de la ruta calculada.
     * @return devuelve resumen de la ruta
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Indica el copyright de la ruta calculada.
     * @return devuelve copyright de la ruta
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * Indica el orden de los hitos incluidos en la ruta.
     * @return devuelve el orden de la ruta
     */
    public ArrayList<Integer> getWaypointIndex() {
        return waypointIndex;
    }

    /**
     * Indica el tiempo total (en segundos) de la ruta calculada (por tramos).
     * @return devuelve el tiempo total de la ruta (en segundos)     
     */
    public ArrayList<Integer> getTotalTime() {
        return totalTime;
    }

    /**
     * Indica las distancia total (en metros) de la ruta calculada (por tramos).
     * @return devuelve la distancia total de la ruta (en metros)
     */
    public ArrayList<Integer> getTotalDistance() {
        return totalDistance;
    }

    /**
     * Indica la referencia de las polilíneas asociadas a cada tramo.
     * @return devuelve referencia de las polilíneas asociadas a cada tramo
     */
    public ArrayList<String> getPolilines() {
        return polilines;
    }

  
    
    @Override
    protected void onError(URL urlRequest, String status, Exception ex) {
        super.storageRequest(urlRequest.toString(), "Route request", status, ex);
    }

    @Override
    protected String getStatus(XPath xpath, Document document) {
        NodeList nodes;
        try {
            nodes = (NodeList) xpath.evaluate(this.pathStatus, 
                document, XPathConstants.NODESET);
            return nodes.item(0).getTextContent();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    protected void storeInfoRequest(URL urlRequest, String info, String status, Exception exception) {
        super.storageRequest(urlRequest.toString(), "Route request", status, exception);
    }
    
    private URL createURL(String originAddress, String destinationAddress, ArrayList<String> waypoints,
            Boolean optimize, mode travelMode,avoids avoidsRoad) throws MalformedURLException, UnsupportedEncodingException{
        
        String origin="?origin=" + URLEncoder.encode(originAddress, "utf-8");
        String destination="&destination=" + URLEncoder.encode(destinationAddress, "utf-8");
        String waypoi="";
        if(waypoints!=null && waypoints.size()>0){
            if(optimize==true){
                waypoi="&waypoints=optimize:true|";
            }else{
                waypoi="&waypoints=";
            }
            for(String item:waypoints){
                waypoi+=URLEncoder.encode(item, "utf-8") + "|";
            }
        }
        String travel="&mode=" + travelMode.toString();
        
        String avoid="";
        switch(avoidsRoad.toString()){
            case "nothing":
                avoid="";
                break;
            case "tolls":
                avoid="&avoid=tolls";
                break;
            case "highways":
                avoid="&avoid=highways";
                break;
        }
        
        URL urlReturn=new URL(URLRoot+origin+destination+waypoi+travel+avoid+super.getSelectPropertiesRequest());
        return urlReturn;
    }
    
    private String[][] getNodesRoute(ArrayList<NodeList> nodes){
        
        String[][] result=new String[1000][5];
        
        for(int i = 0; i < nodes.size();i++){
             for (int j = 0, n = nodes.get(i).getLength(); j < n; j++) {
                String nodeString = nodes.get(i).item(j).getTextContent();
                result[j][i]=nodeString;
             }
        }
        result=(String[][])super.resizeArray(result, nodes.get(0).getLength());
        return result;
    }
    
    private void deleteProperties(){
        summary="";
        copyright="";
        waypointIndex.clear();
        totalTime.clear();
        totalDistance.clear();
        polilines.clear();
    }
    
    /**
     * Devuelve la ruta especificada a partir de de una dirección de origen y una de destino. Además, se 
     * pueden seleccionar puntos intermedios de paso obligado, o hitos (hasta un máximo de 8), y varios tipos de transporte
     * y restricciones de carreteras.
     * @param originAddress dirección postal de origen de la ruta
     * @param destinationAddress dirección postal de destino de la ruta
     * @param waypoints lista de hitos intermedios de paso obligado (opcional)
     * @param optimize reordena (en caso de ser true) los hitos de forma más eficaz para optimizar la ruta proporcionada. Esta optimización es el resultado de aplicar el Problema del viajante.
     * @param travelMode indica el tipo de medio de transporte
     * @param avoidsRoad indica restricción de vías
     * @return devuelve un string bidimensional con la información de la .<br/>
     * @throws java.net.MalformedURLException
     * @throws java.io.UnsupportedEncodingException
     */
    public String[][] getRoute(String originAddress, String destinationAddress, ArrayList<String> waypoints,
            Boolean optimize, mode travelMode,avoids avoidsRoad) throws MalformedURLException, UnsupportedEncodingException{
        deleteProperties();
        URL url=createURL(originAddress,destinationAddress,waypoints,optimize,travelMode,avoidsRoad);
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
            DocumentBuilder builder = factory.newDocumentBuilder(); 
            Document document = builder.parse(url.openStream()); 

            XPathFactory xpathFactory = XPathFactory.newInstance(); 
            XPath xpath = xpathFactory.newXPath(); 

            NodeList nodeLatitude = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/step/start_location/lat", 
                     document, XPathConstants.NODESET);
            NodeList nodeLongitude = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/step/start_location/lng", 
                     document, XPathConstants.NODESET);
            NodeList nodeTime = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/step/duration/text", 
                     document, XPathConstants.NODESET);
            NodeList nodeDistance = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/step/distance/text", 
                     document, XPathConstants.NODESET);
            NodeList nodeIndications = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/step/html_instructions", 
                     document, XPathConstants.NODESET);
            NodeList nodeSummary = (NodeList) xpath.evaluate("DirectionsResponse/route/summary", 
                     document, XPathConstants.NODESET);
            NodeList nodeCopyright = (NodeList) xpath.evaluate("DirectionsResponse/route/copyrights", 
                     document, XPathConstants.NODESET);
            NodeList nodeWaypointsIndex = (NodeList) xpath.evaluate("DirectionsResponse/route/waypoint_index", 
                     document, XPathConstants.NODESET);
            NodeList nodeTotaltime = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/duration/value", 
                     document, XPathConstants.NODESET);
            NodeList nodeTotalDistance = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/distance/value", 
                     document, XPathConstants.NODESET);
            NodeList nodePolilines = (NodeList) xpath.evaluate("DirectionsResponse/route/leg/step/polyline/points", 
                     document, XPathConstants.NODESET);
            
            
            ArrayList<NodeList> allNodes=new ArrayList<>();
            allNodes.add(nodeTime);allNodes.add(nodeDistance);allNodes.add(nodeIndications);
            allNodes.add(nodeLatitude);allNodes.add(nodeLongitude);
            String[][] result=this.getNodesRoute(allNodes);
            this.storeInfoRequest(url, null, this.getStatus(xpath, document), null);
            try {
                this.copyright=nodeCopyright.item(0).getTextContent();
                this.summary=nodeSummary.item(0).getTextContent();
                this.waypointIndex=super.getNodesInteger(nodeWaypointsIndex);
                this.totalDistance=super.getNodesInteger(nodeTotalDistance);
                this.totalTime=super.getNodesInteger(nodeTotaltime);
                this.polilines=super.getNodesString(nodePolilines);
            } catch (DOMException e) {
            }
            
           
            return result;
        } catch (IOException | ParserConfigurationException | XPathExpressionException | SAXException e) {
            onError(url,"NO STATUS",e);
            return null;
        }
    }
}
