/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mapas;

import java.net.URL;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import javafx.event.EventHandler;


/**
 *
 * @author Juampiece
 */
public class Marcador extends Pane  {
    
    
       public WebView Vista = new WebView();
        WebEngine MotorWeb = Vista.getEngine();

        public Marcador(double longitude , double latitude, String nombre) {
            final URL urlGoogleMaps = getClass().getResource("/mapas/mapa2.html");
            MotorWeb.load(urlGoogleMaps.toExternalForm());
            MotorWeb.setOnAlert((WebEvent<String> e) -> {
                System.out.println(e.toString());
            });

            getChildren().add(Vista);

            TextField lati = new TextField(String.valueOf(latitude));
            TextField longi = new TextField(String.valueOf(longitude));
            TextField name = new TextField(nombre);
            Button add = new Button("Añadir Marcador");
                        
            add.setOnAction((ActionEvent arg0) -> {
                double lat = Double.parseDouble(lati.getText());
                double lon = Double.parseDouble(longi.getText());
                String na1 = name.getText();
                MotorWeb.executeScript("crearMarcador("+lat+","+lon+",'"+na1+"')");
                lati.setText("");
                longi.setText("");
                name.setText("");
            });

            HBox toolbar  = new HBox();
            toolbar.getChildren().addAll(new Label("latitud:"),lati,
                    new Label("longitud:"), longi,new Label("nombre:"), 
                    name, add);

            getChildren().addAll(toolbar);

        }

     
    }


        
    
    
    

