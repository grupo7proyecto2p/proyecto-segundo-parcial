/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mapas;

/**
 *
 * @author Juampiece
 */
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * Clase padre desde donde se extienden el resto. Sirve principalmente para asignar/obtener propiedades básicas
 * a la hora de realizar una petición (clave, sensor, idioma, etc.) y para obtener un registro de todas las peticiones realizadas
 */
public abstract class MapsJava {

    //request properties 
    private static int TiempoConec=300;
    private static String Region="ec";
    private static String language="es";
    private static Boolean sensor= Boolean.FALSE;
    private static String APIKey="";
    
    //Stock request
    private static String[][] stockRequest=new String[0][6];

    

    
    //Abstract methods
    protected abstract void onError(URL urlRequest,String status,Exception ex);
    protected abstract String getStatus(XPath xpath, Document document);
    protected abstract void storeInfoRequest(URL urlRequest,String info,String status,Exception exception);
    

    //Protected methods
    private static int numRequest=0;
    protected void storageRequest(String urlRequest,String info,String status,
            Exception exception){
        Date date = new Date();
        numRequest+=1;
        MapsJava.stockRequest=(String[][])(this.resizeArray(MapsJava.stockRequest,numRequest));
        if(MapsJava.stockRequest[numRequest-1]==null){
                MapsJava.stockRequest[numRequest-1]=new String[6];
            }
        MapsJava.stockRequest[numRequest-1][0]=String.valueOf(numRequest);
        MapsJava.stockRequest[numRequest-1][1]=date.toString();
        MapsJava.stockRequest[numRequest-1][2]=status;
        MapsJava.stockRequest[numRequest-1][3]=urlRequest;
        MapsJava.stockRequest[numRequest-1][4]=info;
        if(exception==null){
            MapsJava.stockRequest[numRequest-1][5]="No exception";
        }else{
            MapsJava.stockRequest[numRequest-1][5]=exception.toString();
        }
        
    }
    
    protected String getSelectPropertiesRequest(){
        return "&region=" + MapsJava.Region + "&language=" + MapsJava.language + 
                "&sensor=" + MapsJava.sensor;
    }
     protected ArrayList<String> getNodesString(NodeList node){
         ArrayList<String> result=new ArrayList<>();
             for (int j = 0, n = node.getLength(); j < n; j++) {
                String nodeString = node.item(j).getTextContent();
                result.add(nodeString);
             }
        return result;
    }
     
    protected ArrayList<Double> getNodesDouble(NodeList node){
         ArrayList<Double> result=new ArrayList<>();
             for (int j = 0, n = node.getLength(); j < n; j++) {
                String nodeString = node.item(j).getTextContent();
                result.add(Double.valueOf(nodeString));
             }
        return result;
    }
    
    protected ArrayList<Integer> getNodesInteger(NodeList node){
         ArrayList<Integer> result=new ArrayList<>();
             for (int j = 0, n = node.getLength(); j < n; j++) {
                String nodeString = node.item(j).getTextContent();
                result.add(Integer.valueOf(nodeString));
             }
        return result;
    }
    
    /**
    */
    protected Object resizeArray (Object oldArray, int newSize) {
       int oldSize = java.lang.reflect.Array.getLength(oldArray);
       Class elementType = oldArray.getClass().getComponentType();
       Object newArray = java.lang.reflect.Array.newInstance(
             elementType, newSize);
       int preserveLength = Math.min(oldSize, newSize);
       if (preserveLength > 0)
          System.arraycopy(oldArray, 0, newArray, 0, preserveLength);
       return newArray; 
    }
    
    //Public methods
    /**
     * Comprueba si la clave de desarrollador API Google Maps es válida.
     * @param key clave de desarrollador API Google Maps
     * @return devuelve el estado de una petición con clave API. En caso de ser válida, devuelve
     * "OK", en cualquier otro caso la clave no es correcta.
     * @see MapsJava#setKey(java.lang.String) 
     * @see MapsJava#getKey() 
     */
    public static String APIkeyCheck(String key){
        try{
            URL url=new URL("https://maps.googleapis.com/maps/api/place/search/xml?location=0,0&radius=1000" + 
                    "&sensor=false&key=" + key);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
            DocumentBuilder builder = factory.newDocumentBuilder(); 
            Document document = builder.parse(url.openStream()); 
            XPathFactory xpathFactory = XPathFactory.newInstance(); 
            XPath xpath = xpathFactory.newXPath(); 

            NodeList nodeLatLng = (NodeList) xpath.evaluate("PlaceSearchResponse/status", 
                            document, XPathConstants.NODESET);
            String status = nodeLatLng.item(0).getTextContent();
            return status;
        }catch (Exception e){
            return "NO STATUS";
        }
    }
    
    //Public access to properties of the request (Getters/Setters)
    /**
     * Devuelve el tiempo de conexión máximo (milisegundos) de espera al servidor (NO FUNCIONA)
     * @return int con tiempo máximo de conexión
     * @see MapsJava#setConnectTimeout(int) 
     */
    public static int getConnectTimeout() {
        return TiempoConec;
    }
    /**
     * Establece el tiempo máximo de espera (milisegundos) por el servidor (NO FUNCIONA)
     * @param aConnectTimeout asigna tiempo máximo de conexión
     * @see MapsJava#getConnectTimeout() 
     */
    public static void setConnectTimeout(int aConnectTimeout) {
        TiempoConec = aConnectTimeout;
    }
    
    /**
     * Obtiene la región de búsqueda de resultados (de forma predeterminada "ec").
     * @return devuelve la región de búsqueda actual
     * @see MapsJava#setRegion(java.lang.String) 
     */
    public static String getRegion() {
        return Region;
    }
    
    /**
     * Establece la región de búsqueda de resultados (de forma predeterminada "es").     
     * @param aRegion asigna la región de búsqueda
     * @see MapsJava#getRegion() 
     */
    public static void setRegion(String aRegion) {
        Region = aRegion;
    }

  
    public static String getLanguage() {
        return language;
    }

    public static void setLanguage(String aLanguage) {
        language = aLanguage;
    }

    /**
     * Obtiene si se está utilizando sensor GPS (GNSS) en las peticiones para obtener ubicación (de forma predeterminada es false)
     * @return devuelve true en caso de utilización del sensor y false en caso contrario 
     */
    public static Boolean getSensor() {
        return sensor;
    }
    /**
     * Establece el uso o no uso de un sensor GPS (GNSS) en las peticiones para obtener ubicación (de forma predeterminada false)
     * @param aSensor en caso de ser true, se fuerza a utilizar sensor. Si es false, no se utiliza
     */
    public static void setSensor(Boolean aSensor) {
        sensor = aSensor;
    }
    
    /**
     * Obtiene la clave actual de desarrollador de API Google Maps (sólo necesario para Places)
     * @return obtiene string con clave actual
     */
    public static String getKey() {
        return APIKey;
    }

    /**
     * Establece clave de desarrollador API Google Maps (sólo necesario para Places)
     * @param aKey string con clave API de desarrollador
     */
    public static void setKey(String aKey) {
        APIKey = aKey;
    }
    
    
    //Public acces to stockRequest 
    /**
     * Obtiene registro de todas las peticiones HTTP realizadas. Conforma un String[n][6] con la siguiente
     * @return devuelve un array de dos dimensiones con las diferentes peticiones realizadas
     */
    public static String[][] getStockRequest() {
        return stockRequest;
    }

    /**
     * Obtiene registro de la última petición HTTP realizada. Conforma un String[6] con la siguiente estructura:
     * @return array de una dimensión con la última petición realizada
     */
    public static  String[] getLastRequestRequest() {
        String[] stockRequestTemp=new String[6];
        System.arraycopy(stockRequest[stockRequest.length-1], 0, stockRequestTemp, 0, 6);
        return stockRequestTemp;
    }
    
    /**
     * Obtiene el status de la última petición realizada.
     * @return devuelve string con estado de última petición
     */
    public static String getLastRequestStatus() {
         return stockRequest[stockRequest.length-1][2];
    }
    /**
     * Devuelve URL asociada a la última petición realizada.
     * @return retorna string con URL de la última petición (por ejemplo, "http://maps.google.com/maps/api/geocode/xml?address=Madrid&region=es&language=es&sensor=false"
     */
    public static String getLastRequestURL() {
        return stockRequest[stockRequest.length-1][3];
    }
    /**
     * Devuelve información sobre el tipo de la última petición realizada
     * @return retorna string con información de la última petición realizada (por ejemplo, "Geocoding request")
     */
    public static String getLastRequestInfo() {
         return stockRequest[stockRequest.length-1][4];
    }
    /**
     * Devuelve información sobre la posible excepción generada en la última petición.
     * @return retorna string con información sobre error de la última petición (por ejemplo, "No exception")
     */
    public static String getLastRequestException() {
         return stockRequest[stockRequest.length-1][5];
    }

  
}

