/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces_Graficas;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Joven Ejemplar
 */
public class Aplicacion extends Application {
    private VBox root;
    private MenuPrincipal menu;
    public static Scene scene;
    @Override
    public void start(Stage primaryStage) throws Exception {
        
        //  
        menu=new MenuPrincipal();
        root=menu.getBoxprincipal();

        //Creando el Scene
        scene=new Scene(root,900,600);

        //Seteando el primary Stage
        
        primaryStage.setScene(scene);
        primaryStage.setTitle("Canguro Express S.A");
        primaryStage.show();
    }
    

    public static void main(String[] args){
        launch(args);
    
    
    
    }
    
    
    
   
}
