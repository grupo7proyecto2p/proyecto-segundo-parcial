/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces_Graficas;

import Entidades.Administrador;
import Entidades.Conductor;
import Entidades.Usuario;
import Utilitarios.JsonParser;
import Utilitarios.RegistroDeUsuarios;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Joven Ejemplar
 */
public final class MenuPrincipal {
    private BorderPane borderPane;
    private FlowPane fp;
    private TextField IngresoUsuario;
    private TextField IngresoPass;
    private Label lblUsuario;
    private Label lblPassw;
    private Label lblBienvenida;
    private Button iniciarSesion;
    private Button salir;
    private HBox espacioBotones,contenedorbienvenida;
    private VBox boxprincipal;
    private RegistroDeUsuarios regUsuarios;
    private Usuario user;
    
    
    public MenuPrincipal() {
        
        regUsuarios=new RegistroDeUsuarios();
        JsonParser parser=new JsonParser("src/Archivos/Paquetes.txt");
        try {
            parser.escribirJSON();
        } catch (IOException ex) {
            System.out.println("Ha ocurrido un problema inicializando el archivo de paquetes,estamos trabajando en ello");

        }
        cargarMenuPrincipal();
      
      
  
    }
    
    public void cargarMenuPrincipal(){
        borderPane=new BorderPane();
        cargarCentro();
        cargarBottom();
        cargarTop();
        
        
        borderPane.setTop(contenedorbienvenida);
        borderPane.setCenter(fp);
        borderPane.setBottom(espacioBotones);
        
        
        VBox box=new VBox(borderPane);
        boxprincipal=box;
        boxprincipal.setStyle("-fx-background-color: #A9F5F2");
        boxprincipal.setStyle("-fx-background-image: url('"+getClass().getResource("1234.jpg").toExternalForm()+"');"
                                        + "-fx-background-repeat: stretch;"             
                                        + "-fx-background-position: top center;");
        
        
        
        
    }
    
   
    public void cargarCentro(){
            
        IngresoUsuario=new TextField();
        IngresoPass=new TextField();
        
        lblUsuario=new Label("Por favor,ingrese su usuario:");
        
        
        lblPassw=new Label("Por favor,ingrese su contraseña:");
        
        
        fp=new FlowPane(Orientation.VERTICAL);
        fp.getChildren().add(lblUsuario);
        fp.getChildren().add(IngresoUsuario);
        fp.getChildren().add(lblPassw);
        fp.getChildren().add(IngresoPass);
        fp.setAlignment(Pos.CENTER);
        fp.setVgap(20);
       
       
        
    
    
    }
    public void cargarTop(){
        
        lblBienvenida=new Label("BIENVENIDO A CANGURO EXPRESS S.A");
        lblBienvenida.setMinSize(100,100);
        lblBienvenida.setAlignment(Pos.CENTER);
        contenedorbienvenida=new HBox(lblBienvenida);
        contenedorbienvenida.setAlignment(Pos.CENTER);
    }
    
    
    public void cargarBottom(){
       
        iniciarSesion=new Button("INICIAR SESIÓN");
        salir=new Button("SALIR");
        
        
        //
        iniciarSesion.setOnAction(e ->{
        String correo=IngresoUsuario.getText();
        String pass=IngresoPass.getText();
        user=regUsuarios.validarUsuario(correo,pass);
      if (user instanceof Administrador){
                Scene scene=new Scene((VBox)((Administrador) user).abrirMenu(), 900, 600);
                Stage stage1 = new Stage();
                stage1.setScene(scene);
                stage1.setTitle("Menu Admin");
                stage1.show();
            }else if(user instanceof Conductor){
                Scene sc=new Scene((BorderPane)((Conductor) user).abrirMenu(),900,600);
                
                Stage st = new Stage();
                
                st.setScene(sc);
                st.setTitle("Menu Conductor");
                st.show();
            }
              
            
           
           else{
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Advertencia");
            alert.setHeaderText("!!!Ocurrio un Error!!!");
            alert.setContentText("Usuario o Contrasenia invalido , Ingresar nuevamente ");   
            alert.showAndWait();
            IngresoUsuario.setText("");
            IngresoPass.setText("");
           }
        
            //System.exit(0);
        
        });
        
        
        //
        
        
        //
        
        salir.setOnAction(e->{
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Salir");
            alert.setHeaderText("Desea usted de verdad Salir");
            alert.setContentText("Confirme.");

            ButtonType buttonTypeOne = new ButtonType("Seguro");
            ButtonType buttonTypeCancel = new ButtonType("Cancelar", ButtonData.CANCEL_CLOSE);

            alert.getButtonTypes().setAll(buttonTypeOne,  buttonTypeCancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne){
               Platform.exit();
            }  else {
                
            }
            
        });
        
        //
        
        
        espacioBotones=new HBox(iniciarSesion,salir);
        espacioBotones.setSpacing(30);
        espacioBotones.setAlignment(Pos.CENTER);
    }

    
    public VBox getBoxprincipal() {
        return boxprincipal;
    }
  
    
    
}
