/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces_Graficas;

import Entidades.Conductor;
import Mapas.Marcador;
import Paquetes.Entrega;
import Utilitarios.RegistroDeEntregas;
import java.util.List;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;

/**
 *
 * @author Joven Ejemplar
 */
public class MenuConductor {
    private BorderPane bd;
    private Pane boxContenedor,boxDeEntregas,boxDeEntregados,boxContieneBoxEntregas;
    private WebView boxMapa;
    private List<Entrega> entregas;

    public MenuConductor() {
        
        bd=new BorderPane();
        bd.autosize();
        bd.setTop(crearTop());
        crearCentro();
        
        
        

    }
   
    
    
    public Pane getRoot(){
  
     return bd;
    }
    
    
    public Pane crearTop(){
        
        Label l=new Label("Mis Entregas");
        l.setStyle("-fx-background-color: #000000");
        l.setTextFill(Color.CORNSILK);
        l.setMinSize(25,25);
        HBox textoEntrega=new HBox(l);
        textoEntrega.setStyle("-fx-background-color: #000000");
        textoEntrega.setMinSize(30,30);
        return textoEntrega;
    }
    
    public void crearCentro(){
       
        boxDeEntregas=new VBox();
        boxDeEntregas.autosize();
        boxDeEntregados=new VBox();
        boxDeEntregados.autosize();
        Label ll=new Label("POR ENTREGAR");
        ll.setStyle("-fx-font-weight: bold");
        boxDeEntregas.getChildren().add(ll);
        Label ll2=new Label("ENTREGADOS");
        ll2.setStyle("-fx-font-weight: bold");
        boxDeEntregados.getChildren().add(ll2);
        Marcador m=new Marcador(-2.127494,-79.906687,"Riocentro Norte");
        boxMapa=m.Vista;   //comentario
        boxMapa.autosize();
        
        //Aqui crearé un hbox por cada entrega que contenga la informacion,a este tendré que ponerle 
        //el set on action para que actualice el estado de la entrega hay que hacer el toString de entrega
        
        Thread t1=new Thread(new ActualizadorEntregas());
        t1.start();
        
        
        
        
      
    
    
    
    
    
    }
    
    
    private class ActualizadorEntregas implements Runnable{
        
        @Override
        public void run() {
            

              for(Entrega e: RegistroDeEntregas.paquetesPorEntregar){
                  
                  Label labelEntrega=new Label("Dirección de Entrega");
                  labelEntrega.setStyle("-fx-font-weight: bold");
                  
                  Label labelPaquete=new Label(e.getDireccion()); //OOJO HAY QUE IMPLEMENTAR GETDIRECCION
                  VBox boxEntrega =new VBox(labelEntrega,labelPaquete); 
                  boxEntrega.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
                   
                   
                 
                   
                  boxEntrega.setOnMouseClicked((MouseEvent event) -> {
                      e.setEstado("N");
                      e.setFechaEntrega(String.valueOf(e.getFechaEntrega(e.getSFechaEntrega())));
                      RegistroDeEntregas.paquetesPorEntregar.remove(e);
                      RegistroDeEntregas.paquetesEntregados.add(e);
                    
                      Platform.runLater(()->{
                          boxDeEntregas.getChildren().remove(boxEntrega);
                          boxEntrega.setOnMouseClicked((MouseEvent evet)->{
                              
                          });
                          boxDeEntregados.getChildren().add(boxEntrega);
                      
                      });
                      
                  });
                  boxDeEntregas.getChildren().add(boxEntrega);
                 
                  
        }
                
                  boxContieneBoxEntregas=new VBox(boxDeEntregas,boxDeEntregados);
                  boxContieneBoxEntregas.autosize();
                  boxContenedor=new HBox(boxContieneBoxEntregas,boxMapa);
                  
                  boxContenedor.autosize();
                  Platform.runLater(()->{
                  
                  bd.setCenter(boxContenedor);
                  });
                  
                  
        }
    
    
    
    
    
    }

    public List<Entrega> getEntregas() {
        return entregas;
    }

    public void setEntregas(List<Entrega> entregas) {
        this.entregas = entregas;
    }

    

}
    
    
    
    


       
            
            
  
