/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces_Graficas;

import Paquetes.Entrega;
import java.util.HashSet;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import Entidades.Conductor;
import Entidades.Usuario;
import Mapas.Marcador;
import Utilitarios.JsonParser;
import Utilitarios.RegistroDeEntregas;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import Utilitarios.RegistroDeUsuarios;
import Archivos.GraficasPorDia;
import Archivos.GraficasRecibidaPorRealizada;
import javafx.scene.input.MouseEvent;
import org.json.simple.JSONArray;

/**
 *
 * @author Joven Ejemplar
 */

//creando eel menú administrador
public class MenuAdministrador {
    
   private Button ordenesEntregas;
    private Button entregasEnProceso;
    private Button choferes;
    private ComboBox reportes;
    private Button clean;
    private HBox top;
    private BorderPane rootAdmin;
    private final TableView<Entrega> tableView = new TableView<>();
    private List<Entrega> entregas =RegistroDeEntregas.paquetesPorEntregar;
    private ObservableList<Entrega> data =
            FXCollections.observableArrayList(entregas);
    private VBox rootMid=new VBox();
    RegistroDeUsuarios user = new RegistroDeUsuarios();
    private List<Usuario> conductores = user.getUsuarios();
    
    
    //Método que genera el mapa para los envíos.
    private Map<Conductor,ArrayList<Entrega>> MapaEntrega() {
          Map<Conductor,ArrayList<Entrega>> mapaEntregas = new HashMap<>();
          
          for(Usuario c : conductores){
              ArrayList<Entrega> entregas = new ArrayList<>();
                  for(Entrega e: RegistroDeEntregas.paquetesTotales){
                      if(c.equals(e.getConductor())){
                          entregas.add(e);
                      }
                  }
                mapaEntregas.put((Conductor)c, entregas);
                  
              }
                  
              return mapaEntregas;
          
          
    
}
    public BorderPane getRootAdmin() {
        return rootAdmin;
    }

    




    
    public MenuAdministrador() {
        rootAdmin = new BorderPane();
        crearTop();
        rootAdmin.setTop(top);
        
                
        
    
    }
    
    public void crearTop(){
        top = new HBox();
        ordenesEntregas = new Button("Órdenes");
        ordenesEntregas.setOnMouseClicked(new OrdenesDeEntrega());
        entregasEnProceso = new Button("Envíos");
        entregasEnProceso.setOnMouseClicked(new EntregasEnProceso());
        choferes = new Button("Conductores");
        choferes.setOnMouseClicked(new Choferes());
        ObservableList<String> lista=FXCollections.observableArrayList("Gráficos por Días","Gráficos RxR");
        reportes = new ComboBox();
        reportes.setItems(lista);
        reportes.setOnMouseClicked(new reportes());
        
        clean = new Button("Limpieza de pantalla");
        clean.setDisable(true);
        clean.setOnMouseClicked(e -> {
            rootMid.getChildren().clear();
            clean.setDisable(true);
        });
        
        top.getChildren().addAll(ordenesEntregas, entregasEnProceso, choferes, reportes, clean);
        
        
    }
    
    
    
    
    
    private class OrdenesDeEntrega implements EventHandler{
        
   
        
        
       
        @Override
        public void handle(Event event) {
            //modificar el centro del BorderPane
            //rootMid.getChildren().clear();
        
        
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        //creando las clumnas de la lista pedida

        TableColumn<Entrega, String> tcId = new TableColumn<>("Id");
        tcId.setCellValueFactory(c -> c.getValue().IdProperty());

        TableColumn<Entrega, String> tcCliente = new TableColumn<>("Cliente");
        tcCliente.setCellValueFactory(c -> c.getValue().clienteProperty());

        TableColumn<Entrega, String> tcConductor = new TableColumn<>("Conductor");
        tcConductor.setCellValueFactory(c -> c.getValue().getConductor().getNombre());
        
         TableColumn<Entrega, String> tcEstado = new TableColumn<>("Estado");
        tcEstado.setCellValueFactory(c -> c.getValue().getEstado());

         TableColumn<Entrega, String> tcFechIngreso = new TableColumn<>("Fecha Ingreso");
        tcFechIngreso.setCellValueFactory(c -> c.getValue().fechaIngresoProperty());
        
         TableColumn<Entrega, String> tcFechEntrega = new TableColumn<>("Fecha Entrega");
        tcFechIngreso.setCellValueFactory(c -> c.getValue().fechaEntregaProperty());
        
        //añadiendo las mismas a el TableView
        
        tableView.getColumns().addAll(tcId, tcCliente, tcConductor, tcEstado, tcFechIngreso, tcFechEntrega);
       
                                                                //Entrega.createEntregaList es una lista que aun no se ha puesto en el hilo pero debería estarlo
        FilteredList<Entrega> filteredData = new FilteredList<>(data, p -> true);

        SortedList<Entrega> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tableView.comparatorProperty());

        tableView.setItems(sortedData);
        
        //usando TextFields para hacer la busqueda
        
        //buscador para la fecha de entrega
        Label lsearchFechaEntrega = new Label("Fecha Entrega");
        TextField searchFechaEntrega = new TextField();
        
        searchFechaEntrega.setPromptText("dd/mm/yyyy");
        searchFechaEntrega.textProperty().addListener((prop, old, text) -> {
            filteredData.setPredicate(entrega -> {
                if (text == null || text.isEmpty()) {
                    return true;
                }

                String localEntrega = entrega.getSFechaEntrega().toLowerCase();
                return localEntrega.contains(text.toLowerCase());
            });
        });
        
        //preparando las barras de busqueda de fecha Entrega
         HBox.setHgrow(searchFechaEntrega, Priority.ALWAYS);
        VBox.setVgrow(tableView, Priority.ALWAYS);
        HBox PlocalEntrega = new HBox();
        PlocalEntrega.getChildren().addAll(lsearchFechaEntrega, searchFechaEntrega);
        
        
        //buscador para la fecha de ingreso
        Label lsearchFechaIngreso = new Label("Fecha ingreso");
        TextField searchFechaIngreso = new TextField();
        searchFechaIngreso.setPromptText("dd/mm/yyyy");
        searchFechaIngreso.textProperty().addListener((prop, old, text) -> {
            filteredData.setPredicate(entrega -> {
                if (text == null || text.isEmpty()) {
                    return true;
                }

                String localIngreso = entrega.getSFechaIngreso().toLowerCase();
                return localIngreso.contains(text.toLowerCase());
            });
        });
        
        //preparando las barras de busqueda de fecha Ingreso
         HBox.setHgrow(searchFechaEntrega, Priority.ALWAYS);
        VBox.setVgrow(tableView, Priority.ALWAYS);
        HBox PlocalIngreso = new HBox();
        PlocalIngreso.getChildren().addAll(lsearchFechaIngreso, searchFechaIngreso);
        
        //buscador por Estado de la Entrega
        Label lsearchEstado = new Label("Estado");
        TextField searchEstado = new TextField();
        searchEstado.setPromptText("D - N");
        searchEstado.textProperty().addListener((prop, old, text) -> {
            filteredData.setPredicate(entrega -> {
                if (text == null || text.isEmpty()) {
                    return true;
                }

                String localEstado = entrega.getSEstado().toLowerCase();
                return localEstado.contains(text.toLowerCase());
            });
        });
        
        //preparando las barras de busqueda de Estado
        HBox.setHgrow(searchEstado, Priority.ALWAYS);
        VBox.setVgrow(tableView, Priority.ALWAYS);
        HBox PlocalEstado = new HBox();
        PlocalIngreso.getChildren().addAll(lsearchEstado, searchEstado);
        

        VBox searchBar = new VBox();
        searchBar.getChildren().addAll(PlocalEntrega, PlocalIngreso, PlocalEstado);

        //rootMid = new VBox();
        rootMid.setSpacing(5.0);
        rootMid.setPadding(new Insets(10.0));
        Button nuevoProducto = new Button("Nuevo Producto");
        Button guardar = new Button("Guardar");
        HBox botones= new HBox();
        botones.getChildren().addAll(nuevoProducto, guardar);
        rootMid.getChildren().addAll(searchBar, tableView, botones);
        nuevoProducto.setOnMouseClicked(new nuevoProducto());
        
        rootAdmin.setCenter(rootMid);
        ordenesEntregas.setDisable(true);
        entregasEnProceso.setDisable(false);
        choferes.setDisable(false);
        reportes.setDisable(false);
        clean.setDisable(false);
        
    
    }
    }

    private class EntregasEnProceso implements EventHandler<Event>{

        @Override
        public void handle(Event event) {
            //throw new UnsupportedOperationException("falta rellenar"); //To change body of generated methods, choose Tools | Templates.
           //rootMid.getChildren().clear();
           HBox panelEnvios = new HBox();
           VBox paneUsuarios = new VBox();
           Pane mapa = new Pane();

           MenuAdministrador men = new MenuAdministrador();
           Map<Conductor, ArrayList<Entrega>> mapaEntregasEnProceso = men.MapaEntrega();

           //se procederá a recorrer el mapa para crear tanto los usuarios ccomo las colocacion de los pings
           
           mapaEntregasEnProceso.forEach((k,v) -> { 
               //WebView mapaEntregas = new WebView();
               VBox usuarioP = new VBox();
               Label usuario = new Label();
               usuario.setText(k.getNombre().get());
               usuarioP.getChildren().add(usuario);
               usuario.setAlignment(Pos.CENTER);
               usuarioP.setOnMouseClicked(e -> {
                   System.out.println("hola k ase");
                   v.forEach(E -> {
                       double lat = E.getLat();
                       double lon = E.getLon();
                       Marcador marca = new Marcador(lat,lon,E.getCliente());
                       mapa.getChildren().add(marca);
                       
                   
                   });
                   
               
               });
               paneUsuarios.getChildren().addAll(usuarioP);
               
           });
           
           panelEnvios.getChildren().addAll(paneUsuarios, mapa);
           
           rootAdmin.setCenter(panelEnvios);
           
        ordenesEntregas.setDisable(false);
        entregasEnProceso.setDisable(true);
        choferes.setDisable(false);
        reportes.setDisable(false);
        clean.setDisable(false);
        }
    
    }
    
    private class Choferes implements EventHandler<Event>{

        @Override
        public void handle(Event event) {
            //throw new UnsupportedOperationException("falta rellenar"); //To change body of generated methods, choose Tools | Templates.
            
            rootMid.getChildren().clear();
           
           
           
           
           
           
        ordenesEntregas.setDisable(false);
        entregasEnProceso.setDisable(false);
        choferes.setDisable(true);
        reportes.setDisable(false);
        clean.setDisable(false);
        }
        
    }
    
    private class reportes implements EventHandler<Event>{

        @Override
        public void handle(Event event) {
           
            rootMid.getChildren().clear();           
           
            event.consume();
            //reportes.hide();
            ordenesEntregas.setDisable(false);
            entregasEnProceso.setDisable(false);
            choferes.setDisable(false);
            //reportes.setDisable(false);
            clean.setDisable(false);
        
        
        //leer acerca de crear gráficoos estadisticos en otra pantalla.
        //crear objeto de tipo reporte, para acceder a la informacion.
        
            Pane paneGraficos;
        
            if(reportes.getValue()=="Gráficos por Días"){
                
                rootMid.getChildren().add(new HBox(GraficasPorDia.getGraficaDia()));
            
        
        
            }
            else{
            
               rootMid.getChildren().add(new HBox(GraficasRecibidaPorRealizada.getPane()));
            }
            
            
        //ademas, crear un pane para la pestaña reportes y en los botones
        //crear stages para que se muestren los datos estadisticos.
        }
        
    }
    
    private class nuevoProducto implements EventHandler<Event>{

        @Override
        public void handle(Event event) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
            Stage stageButton = new Stage();
                stageButton.setTitle("Nueva Entrega");
                VBox raiz = new VBox();
                GridPane grid = new GridPane();
                
                grid.setAlignment(Pos.CENTER);
                grid.setHgap(10);
                grid.setVgap(10);
                grid.setPadding(new Insets(25, 25, 25, 25));
                
                //labels
                Label id = new Label("id");
                Label cliente = new Label("cliente");
                Label CedulaConductor = new Label("CedulaConductor");
                Label estado = new Label("estado");
                Label fechaIngreso =new Label("Fecha Ingreso");
                Label fechaEntrega = new Label("Fecha Entrega");
                Label ubicacionLat =new Label("Ubicacion Latitud");
                Label UbicacionLon = new Label("Ubicaacion Longitud");
                Label dimensiones = new Label("Dimensiones");
                
                
                TextField tid = new TextField();
                TextField tcliente = new TextField();
                TextField tCedulaConductor = new TextField();
                TextField testado = new TextField();
                TextField tfechIngreso = new TextField();
                TextField tfechEntrega = new TextField();
                TextField tUbicacionlat= new TextField();
                TextField tfUbicacionLon = new TextField();
                TextField tDimensiones = new TextField();
                
                grid.add(id, 0, 0);
                grid.add(tid, 1, 0);
                grid.add(cliente, 0, 2);
                grid.add(tcliente, 1, 2);
                grid.add(CedulaConductor, 0, 3);
                grid.add(tCedulaConductor, 1, 3);
                grid.add(estado, 0, 4);
                grid.add(testado, 1, 4);
                grid.add(fechaIngreso, 0, 5);
                grid.add(tfechIngreso, 1, 5);
                grid.add(fechaEntrega, 0, 6);
                grid.add(tfechEntrega, 1, 6);
                grid.add(ubicacionLat, 0, 7);
                grid.add(tUbicacionlat, 1, 7);
                grid.add(UbicacionLon, 0, 8);
                grid.add(tfUbicacionLon, 1, 8);
                grid.add(dimensiones, 0, 9);
                grid.add(tDimensiones, 1, 9);
                
                
                Label labelTop = new Label("Nuevo Producto");
                labelTop.setAlignment(Pos.CENTER);
                //seteando los botones:
                Button guardar = new Button("guardar");
                //Button cancelar = new Button("cancelar");
                HBox hb = new HBox();
                hb.setAlignment(Pos.CENTER);
                hb.getChildren().add(guardar);
                raiz.getChildren().addAll(grid, hb);
                
                //se procede a guardar las nuevas entregas
                guardar.setOnMouseClicked(e -> {
                    //entregas
                    Conductor temp = null;
                    for (Usuario c : conductores){

                        if(c instanceof Conductor){                            
                            if (((Conductor) c).getCedula().equals(tCedulaConductor.getText())){
                                temp = (Conductor) c;
                        }

                        }
                    entregas.add(new Entrega(tid.getText(), tcliente.getText(), (Conductor) c, testado.getText(), tfechIngreso.getText(), tfechEntrega.getText(), Double.parseDouble(tUbicacionlat.getText()), Double.parseDouble(tfUbicacionLon.getText()), tDimensiones.getText()));
                    
                }
                    
                    
                });
               
                Scene scene = new Scene(raiz, 450, 350);
                stageButton.setScene(scene);
                
                
                
                
                stageButton.show();
                
            
        }
        }
    
    
    private class webViewer implements EventHandler<Event>{

        Marcador marca;
        double lat;
        double lon;
        double name;

        @Override
        public void handle(Event event) {
            //marca = new Marcador();
            /**double lat = latitud;
            double lon = longitud;
            String name = cliente;
            marca.getEngi().executeScript(
                    "crearMarcador("+lat+","+lon+",'"+name+"')");*/
            
               
            
            
            
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }

        public double getName() {
            return name;
        }

        public void setName(double name) {
            this.name = name;
        }
        
        
        
    }    
    }
   
    
