/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitarios;

import Entidades.Conductor;
import Paquetes.Entrega;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Joven Ejemplar
 */
public class RegistroDeEntregas {
    public static List<Entrega> paquetesEntregados=new ArrayList();
    public static List<Entrega> paquetesPorEntregar=createEntregaList();
    public static List<Entrega> paquetesTotales=createEntregaList();
    
    

    public static List<Entrega> createEntregaList() {
        List<Entrega> entregas = new ArrayList<>();
        // public Entrega(String id, String cliente, Conductor conductor, String estado, String fechaIngreso, String fechaEntrega) {
        
        List<Double> ld=new ArrayList();
         List<List<Double>> l2;
         ld.add(-2.127494);
         ld.add(-79.906687);
         l2=new ArrayList();
         l2.add(ld);      
        Entrega e=new Entrega("654654","Pablo Perez", new Conductor("Ricardo Salazar", 23, "0951041318",l2,"pablope@hotmail.com","pablito"), "D", "25/08/2015", "24/07/2015" , 0.25869, 0.36985, "26x24");   
        e.setDireccion("Riocentro Norte");
        entregas.add(e);
        
            
         List<Double> ld2=new ArrayList();
         List<List<Double>> l3;
         ld2.add(-2.172587);
         ld2.add(-79.899146);
         l3=new ArrayList();
         l3.add(ld);
        Entrega e2=new Entrega("654676","Pablo Rodriguez", new Conductor("Ricardo Salazar", 23, "0981233138",l3,"rodriguez@hotmail.com","soyrodri"), "D", "25/04/2017", "24/07/2018", 0.245698, 0.36587, "26x25" ) ;
        e2.setDireccion("Sauces 8");
        entregas.add(e2);
        
        
         List<Double> ld3=new ArrayList();
         List<List<Double>> l5;
         ld2.add(-2.182454);
         ld2.add(-79.984686);
         l5=new ArrayList();
         l5.add(ld);
        
        Entrega e3=new Entrega("654654","Pedro Perez", new Conductor("Ricardo Salazar", 23, "0951041318",l5,"ricardope@gmail.com","ricardope"), "D", "16/08/2017", "15/09/2017", 0.245698, 0.36587, "26x25");
        e3.setDireccion("Ciudadela Atarazana");
        entregas.add(e3);
        
        
        
        
         List<Double> ld5=new ArrayList();
         List<List<Double>> l6;
         ld5.add(-2.102669);
         ld5.add(-79.980021);
         l6=new ArrayList();
         l6.add(ld);
        Entrega e5=new Entrega("654654","Pablo Perez", new Conductor("Ricardo Salazar", 23, "1234567890",l6,"ricardo@gmail.com","1235"), "D", "25/08/2017", "14/07/2018", 0.245698, 0.36587, "26x25" ); 
        e5.setDireccion("Prosperina");
        entregas.add(e5);
        
        return entregas;
    }

    
    
}
