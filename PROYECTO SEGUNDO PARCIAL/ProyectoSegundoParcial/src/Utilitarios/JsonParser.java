/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitarios;

/**
 *
 * @author Joven Ejemplar
 */

import Paquetes.Entrega;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



public class JsonParser {
    
    private String filename;

    public JsonParser(String filename) {
        this.filename = filename;
    }
    
    
    public JSONArray leerJSON() throws FileNotFoundException, IOException, ParseException{
        
        Path p=Paths.get(filename);
        FileReader in=new FileReader(p.toFile());
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(in);
        JSONArray array = (JSONArray)obj;
        
        return array;
    
    
    }
    
    public void escribirJSON() throws IOException{
        JSONArray list = new JSONArray();
        
    for(Entrega e: RegistroDeEntregas.paquetesTotales){
         String id=e.getId().get();
         String cliente=e.getCliente();
         String conductor=e.getConductor().getNombre().get();
         String estado=e.getSEstado();
         String fechaIngreso=e.getSFechaIngreso();
         String fechaEntrega=e.getSFechaEntrega();
         String[] ss=fechaEntrega.split("/");
         fechaEntrega=ss[0]+' ';
         fechaEntrega+=ss[1]+' ';
         fechaEntrega+=ss[2];
         
         String[] s2=fechaIngreso.split("/");
         fechaIngreso=ss[0]+' ';
         fechaIngreso+=ss[1]+' ';
         fechaIngreso+=ss[2];
         
         double lat=e.getLat();
         double lon=e.getLon();
         String dimensiones=e.getDimensiones();
         JSONObject ob = new JSONObject();
         ob.put("id",id);
         ob.put("cliente",cliente);
         ob.put("conductor",conductor);
         ob.put("estado",estado);
         ob.put("fechaIngreso",fechaIngreso);
         ob.put("fechaEntrega",fechaEntrega);
         ob.put("latitud",lat);
         ob.put("longitud",lon);
         ob.put("dimensiones",dimensiones);
         
         list.add(ob);
    } 
        BufferedWriter out=new BufferedWriter(new FileWriter(filename));
			out.write(list.toJSONString());
                        out.newLine();
			out.flush();
			out.close();
    
    }
    
    
    
}
