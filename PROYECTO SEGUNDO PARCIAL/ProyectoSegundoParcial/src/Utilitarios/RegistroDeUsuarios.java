/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilitarios;


import Entidades.Usuario;
import Entidades.Administrador;
import Entidades.Conductor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joven Ejemplar
 */
public class RegistroDeUsuarios {
    
    
    private List<Usuario> usuarios;
    
    
    

    public RegistroDeUsuarios() {
        
    usuarios=new ArrayList();    
    //Usuario conductorDefecto=new Conductor("Juan Pablo",26,"2345678901");
    Usuario adminDefecto=new Administrador();
    usuarios.add(adminDefecto);
    //usuarios.add(conductorDefecto);
    
    List<Double> ld=new ArrayList();
         List<List<Double>> l2;
         ld.add(-2.127494);
         ld.add(-79.906687);
         l2=new ArrayList();
         l2.add(ld);
         usuarios.add(new Conductor("Juan Pablo Castro", 271, "9874561230",l2,"juanpablo@hotmail.com","jp123"));
         
         
         List<Double> ld2=new ArrayList();
         List<List<Double>> l3;
         ld2.add(-2.172587);
         ld2.add(-79.899146);
         l3=new ArrayList();
         l3.add(ld);
         
         usuarios.add(new Conductor("Ricardo Salazar", 271, "0951041318",l3,"ricardosalas@hotmail.com","otrori"));
         
         List<Double> ld3=new ArrayList();
         List<List<Double>> l5;
         ld2.add(-2.182454);
         ld2.add(-79.984686);
         l5=new ArrayList();
         l5.add(ld);
         
         
         usuarios.add(new Conductor("Sebastian Yugcha", 271, "0000000000",l5,"conductorreeleno@hotmail.com","souotroconduc"));
         
         List<Double> ld5=new ArrayList();
         List<List<Double>> l6;
         ld5.add(-2.102669);
         ld5.add(-79.980021);
         l6=new ArrayList();
         l6.add(ld);
         
         
         
         usuarios.add(new Conductor("José Davalos", 271, "5555555555",l6,"josedav@hotmail.com","josefav"));
         
        
    }
    
    
    
    
    
    public Usuario validarUsuario(String correo,String password){ 
    
    for(Usuario u:usuarios){
        if((u.getCorreo().equals(correo))&&(u.getPassword().equals(password))){
            return u;     
        
        }
    
    }
    
    return null;
    
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    
}
