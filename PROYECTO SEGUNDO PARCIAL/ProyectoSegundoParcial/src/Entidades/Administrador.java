/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import Interfaces_Graficas.MenuAdministrador;

import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Joven Ejemplar
 */
public class Administrador extends Usuario{

    public Administrador() {
        super("ricardo@hotmail.com","ri3");

    }

    public Administrador(String correo, String password) {
        super(correo, password);
    }
    
    @Override
    public Pane abrirMenu(){
        MenuAdministrador menu=new MenuAdministrador();
        VBox box = new VBox();
        
        box.getChildren().add(menu.getRootAdmin());
        return box;
    
    }
}
