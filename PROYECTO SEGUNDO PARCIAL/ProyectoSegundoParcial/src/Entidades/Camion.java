/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author lRichteryu
 */
    public class Camion {
         private String matrícula, placa;
         private Double capacidad;

        public Camion(String matrícula, String placa, Double capacidad) {
            this.matrícula = matrícula;
            this.placa = placa;
            this.capacidad = capacidad;
        }
    
         
        public Camion() {
            
        }

    @Override
    public String toString() {
        return   "matricula=" + matrícula + ", placa=" + placa + ", capacidad=" + capacidad;
    }
        
}

