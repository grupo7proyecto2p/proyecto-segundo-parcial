/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;
import Paquetes.Entrega;
import Interfaces_Graficas.MenuConductor;
import Paquetes.Entrega;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import static javafx.beans.property.BooleanProperty.booleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.layout.Pane;


/**
 *
 * @author Joven Ejemplar
 */
public class Conductor extends Usuario{
    //creados ciertos parámetros necesarios para la busqueda
     private final StringProperty Nombre;
     private final IntegerProperty age; 
     private final BooleanProperty isDisponible = new SimpleBooleanProperty(true);
     private String cedula, foto;
     private Camion camion;
     private List<Entrega> listaEntregas;
     private List<List<Double>> listaZonas;
    
    
   // public Conductor() {  
     //   super("juanpablo@hotmail.com","jp123");
    //}

 /*   public Conductor(String correo, String password) {
        super(correo, password);
    }*/
    
    public Conductor(String Nombre,int age, String cedula,List<List<Double>> lista,String Usuario,String pass){
        super(Usuario,pass);
        this.Nombre  = new SimpleStringProperty(Nombre);
        this.age = new SimpleIntegerProperty(age);
        this.cedula = cedula;
        this.listaZonas=lista;
    }

    

  
     
     @Override
    public Pane abrirMenu(){
        MenuConductor menu=new MenuConductor();
        menu.setEntregas(listaEntregas);
        Pane box=menu.getRoot();
        return box;
    
    }

    public StringProperty getNombre() {
        return Nombre;
    }

        
    public IntegerProperty getAge() {
        return age;
    }

    public BooleanProperty getIsDisponible() {
        return isDisponible;
    }

    
        
       
         
         //en este método hay es en el que se van inicilaizar conductores, ya sea por código quemado
         //o por archivos json.
     

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.cedula);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conductor other = (Conductor) obj;
        if (!Objects.equals(this.cedula, other.cedula)) {
            return false;
        }
        if (!Objects.equals(this.Nombre, other.Nombre)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  "Nombre"+String.valueOf(Nombre)+"age" +String.valueOf(age)+"isDisponible"+String.valueOf(isDisponible)+"cedula"+String.valueOf(cedula)+"foto"+ foto + "camion" + camion;
    }

    public String getCedula() {
        return cedula;
    }

    public List<Entrega> getListaEntregas() {
        return listaEntregas;
    }

    public void setListaEntregas(List<Entrega> listaEntregas) {
        this.listaEntregas = listaEntregas;
    }

    public List<List<Double>> getListaZonas() {
        return listaZonas;
    }

    public void setListaZonas(List<List<Double>> listaZonas) {
        this.listaZonas = listaZonas;
    }
    
    
}

